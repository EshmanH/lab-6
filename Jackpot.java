public class Jackpot{
    public static void main(String[] args) {
        System.out.println("Let's play a game of Jackpot!");
        Board board = new Board();
        boolean gameOver = false;
        int numOfTilesClosed = 0;
        
        while(gameOver == false){
            System.out.println(board);
            if(board.playATurn()){
                gameOver = true;
            } 
            else{
                numOfTilesClosed++;
            }
        }

        if(numOfTilesClosed >= 7){
            System.out.println("NICE, you reached the Jackpot and won the game!");
        } 
        else{
            System.out.println("OOPS, you lost the game!");
        }
    }
}




