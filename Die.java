import java.util.Random;

public class Die{
    private int faceValue;
    private Random r;

    public Die(){
        faceValue = 1;
        r = new Random();
    }

    public int getFaceValue(){
        return faceValue;
    }

    public int roll(){
        faceValue = r.nextInt(6) + 1;
        return faceValue;
    }

    public String toString(){
        return faceValue+"";
    }
}
